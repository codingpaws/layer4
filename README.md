# layer4

layer4 is an object-oriented PHP TCP (and UDP) client manager that abstracts and simplifies network requests for you. 🤵

## Getting started

Install layer4 by running `composer require codingpaws/layer4`.

```php
use CodingPaws\Layer4\ConnectionFactory;

$client = ConnectionFactory::tcp('example.com', '80');

$client->send("GET / HTTP/1.1\n");
$client->send("Host: example.com\n\n");

echo $client->read(1024 * 1024);

// Prints:
//   HTTP/1.1 200 OK
//   ...
```

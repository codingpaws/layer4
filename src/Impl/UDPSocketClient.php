<?php

declare(strict_types=1);

namespace CodingPaws\Layer4\Impl;

use CodingPaws\Layer4\Base\HasSocketInit;
use CodingPaws\Layer4\Base\SimplexClient;
use CodingPaws\Layer4\Exceptions\IllegalStateException;

final class UDPSocketClient extends SimplexClient
{
  use HasSocketInit;

  public function send(string $data): void
  {
    if (!$this->socket) {
      throw new IllegalStateException('Connection is already closed.');
    }
    socket_send($this->socket, $data, strlen($data), 0);
  }

  public function type(): int
  {
    return SOCK_DGRAM;
  }

  public function protocol(): int
  {
    return SOL_UDP;
  }
}

<?php

declare(strict_types=1);

namespace CodingPaws\Layer4\Exceptions;

use Exception;

final class IllegalStateException extends Exception
{
}

<?php

use CodingPaws\Layer4\ConnectionFactory;
use CodingPaws\Layer4\Exceptions\IllegalStateException;
use CodingPaws\Layer4\Impl\UDPSocketClient;
use Tests\Util\UDP;

describe(UDPSocketClient::class, function () {
  subject(fn () => ConnectionFactory::udp("127.0.0.1", 12346));

  before(function () {
    $this->server = UDP::createServer();
  });

  after(function () {
    UDP::closeServer($this->server);
  });

  describe('#close', function () {
    it('closes the socket', function () {
      subject()->close();

      expect(function () {
        subject()->send('some data');
      })->toThrow(IllegalStateException::class);
    });
  });

  describe('#send', function () {
    it('sends data to the server', function () {
      subject()->send('some data');
      socket_recv($this->server, $data, 1024, MSG_DONTWAIT);
      expect($data)->toBe('some data');
    });
  });
});
